
import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;


public class ImageHistogram {
    
    private BufferedImage image;
    private int[] redChannel    = new int[256];;
    private int[] greenChannel  = new int[256];;
    private int[] blueChannel   = new int[256];;
    private int[] alphaChannel  = new int[256];;
    private int[] grayScale     = new int[256];;
    private int[] patterns      = new int[768];
    
    private int RGBToGray(Color c) {
        return (c.getRed() + c.getBlue() + c.getGreen()) / 3;
    }

    public ImageHistogram(BufferedImage image) {

        this.image = image;
        Color c;

        for (int i = 0; i < image.getWidth(); i++) {
            
            for (int j = 0; j < image.getHeight(); j++) {

                c = new Color(image.getRGB(i, j));

                redChannel[c.getRed()] ++;
                greenChannel[c.getGreen()] ++;
                blueChannel[c.getBlue()] ++;
                alphaChannel[c.getAlpha()] ++;
                grayScale[this.RGBToGray(c)] ++;
            }
        }
        
        // El patron es la union de los 3 vectores (histogramas)
        System.arraycopy(redChannel, 0, patterns, 0, redChannel.length);
        System.arraycopy(greenChannel, 0, patterns, redChannel.length, greenChannel.length);
        System.arraycopy(blueChannel,  0, patterns, redChannel.length + greenChannel.length, blueChannel.length);
 
    }
    
    public void drawRedChannel(JPanel panel) {
        
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String serie = "Número de pixeles";

        for (int i = 0; i < redChannel.length; i++) {
            dataset.addValue(redChannel[i], serie, "" + i);
        }

        JFreeChart chart = ChartFactory.createBarChart(
                null, 
                null, 
                null,
                dataset, 
                PlotOrientation.VERTICAL, 
                true, 
                true, 
                false);

        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, Color.RED);
        chart.setAntiAlias(true);
        chart.setBackgroundPaint(new Color(214, 217, 223));
        panel.removeAll();
        panel.repaint();
        panel.setLayout(new java.awt.BorderLayout());
        panel.add(new ChartPanel(chart)); 
        panel.validate(); 
    } 

    public void drawGreenChannel(JPanel panel) {
                
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String serie = "Número de pixeles";

        for (int i = 0; i < greenChannel.length; i++) {
            dataset.addValue(greenChannel[i], serie, "" + i);
        }

        JFreeChart chart = ChartFactory.createBarChart(
                null, 
                null, 
                null,
                dataset, 
                PlotOrientation.VERTICAL, 
                true, 
                true, 
                false);

        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, Color.GREEN);
        chart.setAntiAlias(true);
        chart.setBackgroundPaint(new Color(214, 217, 223));
        panel.removeAll();
        panel.repaint();
        panel.setLayout(new java.awt.BorderLayout());
        panel.add(new ChartPanel(chart));
        panel.validate(); 
    }
    
    public void drawBlueChannel(JPanel panel) {
        
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String serie = "Número de pixeles";

        for (int i = 0; i < blueChannel.length; i++) {
            dataset.addValue(blueChannel[i], serie, "" + i);
        }

        JFreeChart chart = ChartFactory.createBarChart(
                null, 
                null, 
                null,
                dataset, 
                PlotOrientation.VERTICAL, 
                true, 
                true, 
                false);

        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);
        chart.setAntiAlias(true);
        chart.setBackgroundPaint(new Color(214, 217, 223));
        panel.removeAll();
        panel.repaint();
        panel.setLayout(new java.awt.BorderLayout());
        panel.add(new ChartPanel(chart));
        panel.validate(); 
    }
    
    public void drawGrayScale(JPanel panel) {
                
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String serie = "Número de pixeles";

        for (int i = 0; i < grayScale.length; i++) {
            dataset.addValue(grayScale[i], serie, "" + i);
        }

        JFreeChart chart = ChartFactory.createBarChart(
                null, 
                null, 
                null,
                dataset, 
                PlotOrientation.VERTICAL, 
                true, 
                true, 
                false);

        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, Color.GRAY);
        chart.setAntiAlias(true);
        chart.setBackgroundPaint(new Color(214, 217, 223));
        panel.removeAll();
        panel.repaint();
        panel.setLayout(new java.awt.BorderLayout());
        panel.add(new ChartPanel(chart));
        panel.validate(); 
    }
    
    public int[] getRedChannel() {
        return redChannel;
    }

    public int[] getGreenChannel() {
        return greenChannel;
    }

    public int[] getBlueChannel() {
        return blueChannel;
    }

    public int[] getAlphaChannel() {
        return alphaChannel;
    }

    public int[] getGrayScale() {
        return grayScale;
    }

    public int[] getPatterns() {
        return patterns;
    }

    public BufferedImage getImage() {
        return image;
    }
}
