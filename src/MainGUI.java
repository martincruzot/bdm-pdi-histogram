
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

public class MainGUI extends javax.swing.JFrame {

    private BufferedImage bufferedImage;
    private ImageHistogram histogram;
    private ImageHistogram histSearch;
    private FileLinkedList linkedList;
    private Vector<Register> vectMatching;

    public static final String PATH_BASE = "database/";

    public MainGUI() {

        try {
            linkedList = new FileLinkedList(new File("patterns.dat"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        initComponents();
        lblSimilitud.setText(new Integer(sliderSimilitud.getValue()).toString());
        lblError.setText(new Integer(sliderError.getValue()).toString());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        btnAddDataBase = new javax.swing.JButton();
        sliderError = new javax.swing.JSlider();
        sliderSimilitud = new javax.swing.JSlider();
        lblError = new javax.swing.JLabel();
        lblSimilitud = new javax.swing.JLabel();
        btnSearchImage = new javax.swing.JButton();
        pnlHistogramOriginal = new javax.swing.JPanel();
        rbtRedChannel = new javax.swing.JRadioButton();
        rbtGreenChannel = new javax.swing.JRadioButton();
        rbtBlueChannel = new javax.swing.JRadioButton();
        rbtGrayScale = new javax.swing.JRadioButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        lblLoadedImage = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblResults = new javax.swing.JTable();
        pnlHistogramSearch = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        lblSelectImage = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuItemOpenImage = new javax.swing.JMenuItem();
        menuItemExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuItemAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Parametros de búsqueda", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 13), new java.awt.Color(0, 102, 255))); // NOI18N
        jPanel2.setToolTipText("");
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnAddDataBase.setText("Agrega a Base de Datos");
        btnAddDataBase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddDataBaseActionPerformed(evt);
            }
        });
        jPanel2.add(btnAddDataBase, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 158, 182, 30));

        sliderError.setValue(30);
        sliderError.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Porcentaje de error", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 102, 255))); // NOI18N
        sliderError.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                sliderErrorMouseDragged(evt);
            }
        });
        sliderError.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                sliderErrorMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sliderErrorMouseReleased(evt);
            }
        });
        jPanel2.add(sliderError, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 37, 242, -1));

        sliderSimilitud.setValue(20);
        sliderSimilitud.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Porcentaje de similitud", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 102, 255))); // NOI18N
        sliderSimilitud.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                sliderSimilitudMouseDragged(evt);
            }
        });
        sliderSimilitud.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                sliderSimilitudMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sliderSimilitudMouseReleased(evt);
            }
        });
        jPanel2.add(sliderSimilitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 94, 242, -1));

        lblError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblError.setText("0");
        lblError.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.add(lblError, new org.netbeans.lib.awtextra.AbsoluteConstraints(284, 54, 31, -1));

        lblSimilitud.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSimilitud.setText("0");
        lblSimilitud.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.add(lblSimilitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(284, 105, 31, -1));

        btnSearchImage.setText("Buscar");
        btnSearchImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchImageActionPerformed(evt);
            }
        });
        jPanel2.add(btnSearchImage, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 206, 182, 29));

        pnlHistogramOriginal.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout pnlHistogramOriginalLayout = new javax.swing.GroupLayout(pnlHistogramOriginal);
        pnlHistogramOriginal.setLayout(pnlHistogramOriginalLayout);
        pnlHistogramOriginalLayout.setHorizontalGroup(
            pnlHistogramOriginalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 366, Short.MAX_VALUE)
        );
        pnlHistogramOriginalLayout.setVerticalGroup(
            pnlHistogramOriginalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel2.add(pnlHistogramOriginal, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 20, 370, 230));

        buttonGroup1.add(rbtRedChannel);
        rbtRedChannel.setSelected(true);
        rbtRedChannel.setText("Red Channel");
        rbtRedChannel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbtRedChannelMouseClicked(evt);
            }
        });
        jPanel2.add(rbtRedChannel, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 150, -1, -1));

        buttonGroup1.add(rbtGreenChannel);
        rbtGreenChannel.setText("Green Channel");
        rbtGreenChannel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbtGreenChannelMouseClicked(evt);
            }
        });
        jPanel2.add(rbtGreenChannel, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 170, -1, -1));

        buttonGroup1.add(rbtBlueChannel);
        rbtBlueChannel.setText("Blue Channel");
        rbtBlueChannel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbtBlueChannelMouseClicked(evt);
            }
        });
        jPanel2.add(rbtBlueChannel, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 190, -1, -1));

        buttonGroup1.add(rbtGrayScale);
        rbtGrayScale.setText("Gray Scale");
        rbtGrayScale.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbtGrayScaleMouseClicked(evt);
            }
        });
        jPanel2.add(rbtGrayScale, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 210, -1, -1));

        lblLoadedImage.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jScrollPane2.setViewportView(lblLoadedImage);

        jPanel2.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 20, 350, 230));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resultados", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 13), new java.awt.Color(51, 102, 255))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblResults.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Indice", "Nombre"
            }
        ));
        tblResults.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblResultsMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblResults);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 320, 240));

        pnlHistogramSearch.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout pnlHistogramSearchLayout = new javax.swing.GroupLayout(pnlHistogramSearch);
        pnlHistogramSearch.setLayout(pnlHistogramSearchLayout);
        pnlHistogramSearchLayout.setHorizontalGroup(
            pnlHistogramSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 366, Short.MAX_VALUE)
        );
        pnlHistogramSearchLayout.setVerticalGroup(
            pnlHistogramSearchLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 236, Short.MAX_VALUE)
        );

        jPanel1.add(pnlHistogramSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 20, 370, 240));

        lblSelectImage.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jScrollPane3.setViewportView(lblSelectImage);

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 20, 350, 240));

        jMenu1.setText("Archivo");

        menuItemOpenImage.setText("Abrir imagen");
        menuItemOpenImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemOpenImageActionPerformed(evt);
            }
        });
        jMenu1.add(menuItemOpenImage);

        menuItemExit.setText("Salir");
        menuItemExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemExitActionPerformed(evt);
            }
        });
        jMenu1.add(menuItemExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Ayuda");

        menuItemAbout.setText("Acerca de");
        jMenu2.add(menuItemAbout);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 1080, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuItemOpenImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemOpenImageActionPerformed

        JFileChooser chooser = new JFileChooser();
        int value = chooser.showOpenDialog(null);

        if (value == JFileChooser.APPROVE_OPTION) {
            File fileSelect = chooser.getSelectedFile();
            String path = fileSelect.getPath();

            // Borramos datos anteriores en el Frame
            this.cleanFields();

            //Poner vista previa de la imagen
            ImageIcon img = new ImageIcon(path);
            Icon scaleImage = new ImageIcon(img.getImage().getScaledInstance(lblLoadedImage.getWidth(), lblLoadedImage.getHeight(), Image.SCALE_DEFAULT));
            lblLoadedImage.setIcon(scaleImage);
            this.repaint();

            // Cargar la imagen en buffer
            try {
                bufferedImage = ImageIO.read(new File(path));
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            // Crean y muestra histograma
            histogram = new ImageHistogram(bufferedImage);
            this.showHistogram(this.histogram, this.pnlHistogramOriginal);
        }
    }//GEN-LAST:event_menuItemOpenImageActionPerformed


    private void tblResultsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultsMousePressed

        int row = tblResults.getSelectedRow();
        showDataRow(row);
    }//GEN-LAST:event_tblResultsMousePressed

    private void btnSearchImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchImageActionPerformed
        
        // Borrando resultados anteriores
        this.lblSelectImage.setIcon(null);
        this.pnlHistogramSearch.removeAll();
        String[] title = {"Indice", "Nombre"};
        tblResults.setModel(new DefaultTableModel(null, title));
        this.repaint();
        
        if (bufferedImage != null) {
            try {
                Register x = new Register(histogram.getPatterns());
                int error = sliderError.getValue();
                int similitud = sliderSimilitud.getValue();
                vectMatching = linkedList.getMatchingRegisters(x, error, similitud);

                // Llenar valores en una tabla
                this.showTable(vectMatching);
                
                if (vectMatching.isEmpty()) {
                    JOptionPane.showMessageDialog(this, "No se encontrado coincidencias!");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(this, "No se ha cargado imagen!");
        }

    }//GEN-LAST:event_btnSearchImageActionPerformed

    private void sliderSimilitudMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderSimilitudMouseReleased
        lblSimilitud.setText(new Integer(sliderSimilitud.getValue()).toString());
    }//GEN-LAST:event_sliderSimilitudMouseReleased

    private void sliderSimilitudMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderSimilitudMousePressed
        lblSimilitud.setText(new Integer(sliderSimilitud.getValue()).toString());
    }//GEN-LAST:event_sliderSimilitudMousePressed

    private void sliderSimilitudMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderSimilitudMouseDragged
        lblSimilitud.setText(new Integer(sliderSimilitud.getValue()).toString());
    }//GEN-LAST:event_sliderSimilitudMouseDragged

    private void sliderErrorMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderErrorMouseReleased
        lblError.setText(new Integer(sliderError.getValue()).toString());
    }//GEN-LAST:event_sliderErrorMouseReleased

    private void sliderErrorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderErrorMousePressed
        lblError.setText(new Integer(sliderError.getValue()).toString());
    }//GEN-LAST:event_sliderErrorMousePressed

    private void sliderErrorMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderErrorMouseDragged
        lblError.setText(new Integer(sliderError.getValue()).toString());
    }//GEN-LAST:event_sliderErrorMouseDragged

    private void btnAddDataBaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddDataBaseActionPerformed
        if (bufferedImage != null) {
            try {
                // Guardar imagen en un directorio
                int numRegistro = linkedList.numRegisters() + 1;
                String namephoto = PATH_BASE + Integer.toString(numRegistro) + ".jpg";
                this.saveImage(bufferedImage, namephoto);

                // Guardando registro en archivo
                linkedList.writeRegister( new Register(histogram.getPatterns() ));
                linkedList.showRegisters();
                
                JOptionPane.showMessageDialog(this, "Se ha guardado exitosamente!!");

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        else {
            JOptionPane.showMessageDialog(this, "No se ha cargado imagen!");
        }


    }//GEN-LAST:event_btnAddDataBaseActionPerformed

    private void rbtGreenChannelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbtGreenChannelMouseClicked
        this.showHistogram(this.histogram, this.pnlHistogramOriginal);
        this.showHistogram(this.histSearch, this.pnlHistogramSearch);
    }//GEN-LAST:event_rbtGreenChannelMouseClicked

    private void rbtBlueChannelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbtBlueChannelMouseClicked
        this.showHistogram(this.histogram, this.pnlHistogramOriginal);
        this.showHistogram(this.histSearch, this.pnlHistogramSearch);
    }//GEN-LAST:event_rbtBlueChannelMouseClicked

    private void rbtRedChannelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbtRedChannelMouseClicked
        this.showHistogram(this.histogram, this.pnlHistogramOriginal);
        this.showHistogram(this.histSearch, this.pnlHistogramSearch);

    }//GEN-LAST:event_rbtRedChannelMouseClicked

    private void rbtGrayScaleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbtGrayScaleMouseClicked
        this.showHistogram(this.histogram, this.pnlHistogramOriginal);
        this.showHistogram(this.histSearch, this.pnlHistogramSearch);
    }//GEN-LAST:event_rbtGrayScaleMouseClicked

    private void menuItemExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_menuItemExitActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainGUI().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddDataBase;
    private javax.swing.JButton btnSearchImage;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblError;
    private javax.swing.JLabel lblLoadedImage;
    private javax.swing.JLabel lblSelectImage;
    private javax.swing.JLabel lblSimilitud;
    private javax.swing.JMenuItem menuItemAbout;
    private javax.swing.JMenuItem menuItemExit;
    private javax.swing.JMenuItem menuItemOpenImage;
    private javax.swing.JPanel pnlHistogramOriginal;
    private javax.swing.JPanel pnlHistogramSearch;
    private javax.swing.JRadioButton rbtBlueChannel;
    private javax.swing.JRadioButton rbtGrayScale;
    private javax.swing.JRadioButton rbtGreenChannel;
    private javax.swing.JRadioButton rbtRedChannel;
    private javax.swing.JSlider sliderError;
    private javax.swing.JSlider sliderSimilitud;
    private javax.swing.JTable tblResults;
    // End of variables declaration//GEN-END:variables

    private void showTable(Vector<Register> v) {

        String[] title = {"Indice", "Nombre"};
        String[] rowsTable = new String[100];
        DefaultTableModel tableModel = new DefaultTableModel(null, title);

        for (int i = 0; i < v.size(); i++) {
            rowsTable[0] = Integer.toString(v.get(i).getIndex());
            rowsTable[1] = Integer.toString(v.get(i).getIndex()) + ".jpg";
            tableModel.addRow(rowsTable);
        }

        tblResults.setModel(tableModel);
    }

    private void showDataRow(int row) {
        String path = this.PATH_BASE + Integer.toString(vectMatching.get(row).getIndex()) + ".jpg";
        System.out.println("Path: " + path);

        ImageIcon img = new ImageIcon(path);
        Icon imagenEscalada = new ImageIcon(img.getImage().getScaledInstance(lblSelectImage.getWidth(), lblSelectImage.getHeight(), Image.SCALE_DEFAULT));
        lblSelectImage.setIcon(imagenEscalada);
        this.repaint();

        try {
            histSearch = new ImageHistogram(ImageIO.read(new File(path)));
            this.showHistogram(histSearch, pnlHistogramSearch);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void showHistogram(ImageHistogram hist, JPanel panel) {
        if (hist != null) {

            if (rbtRedChannel.isSelected()) {
                hist.drawRedChannel(panel);
            } else if (rbtGreenChannel.isSelected()) {
                hist.drawGreenChannel(panel);
            } else if (rbtBlueChannel.isSelected()) {
                hist.drawBlueChannel(panel);
            } else if (rbtGrayScale.isSelected()) {
                hist.drawGrayScale(panel);
            }
            this.repaint();
        } 
    }

    private void saveImage(BufferedImage img, String path) throws IOException {
        File file = new File(PATH_BASE);
        
        if ( !file.isDirectory() ) {
            System.out.println("El directorio de imagenes no existe!");
            file.mkdirs();
        } 
        ImageIO.write(img, "JPG", new File(path));
    }

    private void cleanFields() {
        this.lblLoadedImage.setIcon(null);
        this.lblSelectImage.setIcon(null);
        this.pnlHistogramOriginal.removeAll();
        this.pnlHistogramSearch.removeAll();
        String[] title = {"Indice", "Nombre"};
        tblResults.setModel(new DefaultTableModel(null, title));
        this.repaint();
    }
}
