import java.util.Arrays;

public class Register {
    
    private int index = 0;
    private int next = -1;
    private int[] patterns = new int[768];  
    String namePhoto;
    
    public Register() {
    }
    
    public Register(int[] patterns) {
        this.patterns = patterns;
    }

    public int[] getPatterns() {
        return this.patterns;
    }

    public void setPatterns(int[] patterns) {
        this.patterns = patterns;
    }

    public int getPattern(int index) {
        return patterns[index];
    }

    public void setPattern(int index, int value) {
        this.patterns[index] = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }

    public String getNamePhoto() {
        return namePhoto;
    }

    public void setNamePhoto(String namePhoto) {
        this.namePhoto = namePhoto;
    }

    public int getBytesSize() {
        return ( patterns.length * FileLinkedList.SIZE_INT) + (2 * FileLinkedList.SIZE_INT) ;
    }
    
    public int compare(Register reg) {
        
        for (int i = 0; i < 9; i++) {
            if (patterns[i] > reg.getPattern(i)) {
                return 1;
            }
            else if (patterns[i] < reg.getPattern(i)) {
                return -1; 
            }
        }
        return 0;
    }
    
    public void show(String message) {
        System.out.print(message);
        System.out.print(this.index + "\t" + this.next + "\t");
        System.out.println(Arrays.toString(this.patterns));
    }
}