
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.util.Vector;

public class FileLinkedList {

    public static final int SIZE_INT = 4;
    private static final int EOF = -1;

    private class Header {

        public int numRegs = 0;
        public int first = -1;
        public int last = -1;

        public int getBytesSize() {
            return (3 * SIZE_INT);
        }

        public void showData() {
            System.out.println( "\nNUMREGS: " + numRegs + 
                                "\tFIRST : " + first + 
                                "\tLAST  : " + last);
        }
    }

    private RandomAccessFile accessFile;
    private File file;
    private Header header = new Header();
    
    // Punteros manejo d eregistros
    private int nextRegister = 1;

    public FileLinkedList(File file) throws IOException {
        this.file = file;
        this.initFile();
    }

    private void initFile() throws IOException {
        accessFile = new RandomAccessFile(file, "rw");

        if (accessFile.length() == 0) {
            accessFile.writeInt(header.numRegs);
            accessFile.writeInt(header.first);
            accessFile.writeInt(header.last);
        }

        accessFile.close();
    }
    
    public Vector<Register> getMatchingRegisters(Register reg, int porcentajeError, int similitud) throws IOException{
        
        Vector<Register> vector = new Vector<Register>();
        accessFile = new RandomAccessFile(file, "r");
        
        header.numRegs = accessFile.readInt();
        header.first = accessFile.readInt();
        header.last = accessFile.readInt();
        
        for (int i = 0; i < header.numRegs; i++) {
            Register x = readLine(accessFile);
            
            if ( matching(reg.getPatterns(), x.getPatterns(), porcentajeError, similitud) ) {
                vector.add(x);
                System.out.println("\nMATCHING!");
                reg.show("Original:\t");
                x.show("Registro " + x.getIndex() + ":\t");
                
            }
        }
        System.out.println("Busqueda terminada!");
        accessFile.close();
        return vector;
    }
    
    public boolean matching(int[] p1, int[] p2, int porcentajeError, int similitud) {
 
        if ( calculaError(p1, p2, similitud) < porcentajeError) {
            return true;
        } else {
            return false;
        }
    }

    public float calculaError(int[] patron1, int[] patron2, int deltaSimilitud) {

        int   sumaValores = 0;
        float porcentaje = 0;           // [0- 100]
        int[] valores = new int[9];
        int delta = 0;

        // Verificando diferencia en similitud
        for (int i = 0; i < 9; i++) {
           
            delta =   Math.abs(patron1[i] - patron2[i]);

            if (delta <= deltaSimilitud) {
                valores[i] = 0;
            } else {
                valores[i] = 1;
            }
        }

        // Calculando porcentaje 
        for (int i = 0; i < 9; i++) {
            sumaValores += valores[i];
        }

        porcentaje = (sumaValores * 100) / 9;
        return porcentaje;
    }
        
        
    public void writeRegister(Register reg) throws IOException {

        accessFile = new RandomAccessFile(file, "rw");
        int sizeHead = header.getBytesSize();
        int sizeReg = reg.getBytesSize();

        // Leer cabezera
        header.numRegs = accessFile.readInt();
        header.first = accessFile.readInt();
        header.last = accessFile.readInt();

        // Inicializar valores de registro
        reg.setIndex(++header.numRegs);
        int band = 0;
        Register next, prev = new Register();
        long position;

        if (header.first == -1) {  // Lista vacia
            reg.setNext(header.first);
            header.first = reg.getIndex();

        } else {
            int x = header.first;

            while (x != EOF) {
                position = (x - 1) * sizeReg + sizeHead;
                accessFile.seek(position);
                next = readLine(accessFile);

                if (reg.compare(next) > 0) {
                    band = 1;
                    prev = next;
                    x = next.getNext();
                    continue;
                } else {
                    break;
                }
            }

            if (band == 0) {
                reg.setNext(header.first);
                header.first = reg.getIndex();
            }
            if (band == 1) {
                reg.setNext(prev.getNext());
                prev.setNext(reg.getIndex());
                position = (prev.getIndex() - 1) * sizeReg + sizeHead;
                accessFile.seek(position);
                writeLine(prev, accessFile);
            }
        }

        // Actualizar cabezera
        accessFile.seek(0);
        accessFile.writeInt(header.numRegs);
        accessFile.writeInt(header.first);
        accessFile.writeInt(header.last);

        // Insertar registro nuevo
        position = (header.numRegs - 1) * sizeReg + sizeHead;
        accessFile.seek(position);
        writeLine(reg, accessFile);

        accessFile.close();
    }

    private void writeLine(Register reg, RandomAccessFile raf) throws IOException {

        raf.writeInt(reg.getIndex());
        raf.writeInt(reg.getNext());

        for (int i = 0; i < reg.getPatterns().length; i++) {
            raf.writeInt(reg.getPattern(i));
        }
    }

    private Register readLine(RandomAccessFile raf) throws IOException {
        Register reg = new Register();

        reg.setIndex(raf.readInt());
        reg.setNext(raf.readInt());

        for (int i = 0; i < reg.getPatterns().length; i++) {
            reg.setPattern(i, raf.readInt());
        }
        
        return reg;
    }

    public void showRegisters() throws IOException {
        accessFile = new RandomAccessFile(file, "rw");

        // Leer cabezera
        header.numRegs = accessFile.readInt();
        header.first = accessFile.readInt();
        header.last = accessFile.readInt();
        header.showData();
        
        System.out.println("Listado de regitros");
        for (int i = 0; i < header.numRegs; i++) {
            Register it = readLine(accessFile);
            it.show("");
        }

        accessFile.close();
    }
    
    public int numRegisters() throws IOException {
        accessFile = new RandomAccessFile(file, "r");
        header.numRegs = accessFile.readInt();
        accessFile.close();
        
        return this.header.numRegs;
    }
    
    public int indexFirtRegister() throws IOException{
        accessFile = new RandomAccessFile(file, "r");
        header.numRegs = accessFile.readInt();
        header.first = accessFile.readInt();
        accessFile.close();
   
        return this.header.first;
    }
    
    
}
